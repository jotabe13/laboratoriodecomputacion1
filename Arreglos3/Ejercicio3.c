#include <stdio.h>

int main(int argc, char *argv[]) {
	int a[15]=  {5,4,10,8,2,11,9,1};
	int valor=0, posicion=0;
	printf("Ingresar el valor que desea agregar: \n"); // Se ingresa el valor a agregar
	scanf("%d", &valor);
	printf("Ingresar la posicion que desea agregar: \n"); //Se ingresa la posicion a la cual se desea a�adir el valor
	scanf("%d", &posicion);
	
	if(posicion>14)printf("La posicion ingresada no se encuentra dentro de los limites del arreglo\n"); //Control para evitar que se ingrese una posicion que no existe
	else a[posicion]=valor; // Se a�ade el valor en la posicion
	
	printf("El arreglo final es el siguiente: \n"); //Se imprime el arreglo
	for(int i=0; i<15; i++){
		printf("%d/", a[i]);
	}
	
	
	
	return 0;
}
