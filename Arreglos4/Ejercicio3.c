#include <stdio.h>

int main(int argc, char *argv[]) {
	int n=10,j=0,aux=0, cont=0;
	int a[10]={1,2,3,4,5,6,7,8,9,10}, multiplos[10]={0,0,0,0,0,0,0,0,0,0}, no_multiplos[10]={0,0,0,0,0,0,0,0,0,0};
	for(int i=0; i<10; i++){
		if(a[i]%3==0){
			multiplos[cont]=a[i];
			cont++;
		}else no_multiplos[i]=a[i];
		
	}
	printf("El vector base es:\n");
	for(int i=0;i<10;i++){
		printf("%d/", a[i]);
	}
	
	printf("\nEl vector de multiplos es: \n");
	for(int i=0;i<10;i++){
		printf("%d/", multiplos[i]);
	}
	
	printf("\nEl vector de no multiplos es: \n");
	for(int i=0;i<10;i++){
		if(no_multiplos[i]!=0) printf("%d/", no_multiplos[i]);
	}
	
	
	printf("\nEl arreglo completo de multiplos es: \n");
	for(int i=0;i<10;i++){
		if(multiplos[i]==0){
			multiplos[i]=multiplos[i-1]+3;
		}
		printf("%d/", multiplos[i]);
	}
	
	
	for(int i=1;i<n;i++)
	{
		j=i;
		aux=multiplos[i];
		while(j>0 && aux<multiplos[j-1])
		{
			multiplos[j]=multiplos[j-1];
			j--;
		}
		multiplos[j]=aux;
	}
	printf("\nEl arreglo de multiplos ordenado es:\n");
	for(int i=0;i<10;i++)
	{
		printf("%d/",multiplos[i]);
	}
	
	
	
	
	
	return 0;
}
