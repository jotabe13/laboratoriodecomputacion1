#include <stdio.h>
#include <conio.h>


int triangulo(int fila, int columna){
	
	if (columna == 0 || fila == columna) return 1; 
	else return (triangulo((fila - 1),(columna - 1)) + triangulo((fila-1), columna)); 
}

int main (){
	
	int grado;
	
	printf(" Ingrese el grado del binomio o numero de fila: ");
	scanf("%d", &grado);
	
	for (int i = 0; i <= grado; i++){
		
		for (int j = 0; j <= i; j++){
			printf (" %d",triangulo(i, j)); 
		}
		
		printf("\n");
	}
}
