#include <stdio.h> // Para printf
#include <math.h> // Para floor

// Prototipos de las funciones
int busquedaBinariaRecursiva(int arreglo[], int busqueda, int izquierda, int derecha);


int main(){
	int numeros[] = {1, 2, 4, 8, 15, 16, 20, 50};
	int busqueda = 10;
	int longitudDelArreglo = sizeof(numeros) / sizeof(numeros[0]);
	int resultadoBusquedaRecursiva = busquedaBinariaRecursiva(numeros, busqueda, 0, longitudDelArreglo - 1);
	printf("Al buscar %d recursivamente, el resultado es %d\n", busqueda, resultadoBusquedaRecursiva);

}


int busquedaBinariaRecursiva(int arreglo[], int busqueda, int izquierda, int derecha){
	if (izquierda > derecha) return -1;
	
	int indiceDeLaMitad = floor((izquierda + derecha) / 2);
	
	int valorQueEstaEnElMedio = arreglo[indiceDeLaMitad];
	if (busqueda == valorQueEstaEnElMedio){
		return indiceDeLaMitad;
	}
	
	if (busqueda < valorQueEstaEnElMedio){
		// Entonces est� hacia la izquierda
		derecha = indiceDeLaMitad - 1;
	}else{
		// Est� hacia la derecha
		izquierda = indiceDeLaMitad + 1;
	}
	return busquedaBinariaRecursiva(arreglo, busqueda, izquierda, derecha);
}

