#include <stdio.h>
#include <stdlib.h>


int funcionQueCompara(const void *a, const void *b) {
	int aInt = *(int *) a;
	int bInt = *(int *) b;
	
	return aInt - bInt;
}


int main(void) {
	
	int lista1[10]={10,9,8,7,6,5,4,3,2,1};
	int lista2[10]={20,19,18,17,16,15,14,13,12,21};
	int total[20];
	
	
	for(int i=0;i<20;i++){
		total[i]=lista1[i];
		total[i]=lista2[i];
	}
	int tamanioElemento = sizeof total[0];
	int longitud = sizeof total / tamanioElemento;
	printf("Imprimendo arreglo antes de ordenar\n");
	for (int x = 0; x < longitud; x++) {
		printf("%d ", total[x]);
	}
	qsort(total, longitud, tamanioElemento, funcionQueCompara);
	
	printf("\nImprimendo arreglo ya ordenado\n");
	for (int x = 0; x < longitud; x++) {
		printf("%d ", total[x]);
	}
	return 0;
}
