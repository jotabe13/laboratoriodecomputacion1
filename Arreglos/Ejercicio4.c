#include <stdio.h>

int main(int argc, char *argv[]) {
	
	int lista[10]={1,2,3,4,5,6,7,8,9,10}, multiplos[10]={0,0,0,0,0,0,0,0,0,0}, no_multiplos[10]={0,0,0,0,0,0,0,0,0,0};
	for(int i=0; i<10; i++){
		if(lista[i]%3==0) multiplos[i]=lista[i];
		else no_multiplos[i]=lista[i];
	}
	printf("El vector base es:\n");
	for(int i=0;i<10;i++){
		printf("%d\n", lista[i]);
	}
	printf("El vector de multiplos es: \n");
	for(int i=0;i<10;i++){
		if(multiplos[i]!=0) printf("%d\n", multiplos[i]);
		
	}
	printf("El vector de no multiplos es: \n");
	for(int i=0;i<10;i++){
		if(no_multiplos[i]!=0) printf("%d\n", no_multiplos[i]);
	}
	
	return 0;
}

