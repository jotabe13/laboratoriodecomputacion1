#include <stdio.h>

int main() {
	//Se crean las variables
	int a, resultado;
	a=0;
	resultado=0;
	// Se pide al usuario que ingrese un numero
	printf("Ingrese un numero: ");
	scanf("%d", &a);
	//Se escribe la condicion
	if (a%2==0&&a>0){
		//Si se cumple la condicion, realiza la operacion
		resultado=a*3;
		printf("El resultado es: %d", resultado);
	} else{
		//Sino, muestra por pantalla el mensaje de error
		printf("No se ha podido realizar la operacion");
	}
	return 0;
}

