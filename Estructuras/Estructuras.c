#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	struct Ejercicio1
	{
		char dni[10];
		char apellido[20];
		char nombre[20];
		int edad;
		float promedio;
	};
	
	struct Ejercicio1 ejercicio[10];
	
	int total=0,total_edad=0,mayor=0,menor=99999999,prom_mayor=0,prom_menor=0,porcentaje=0;
	float prom=0,prom_notas,prom_alumnos=0;
	
	for(int i=0;i<3;i++){
		printf("Alumno N� %d\n",i+1);
		printf( "Ingrese el DNI del alumno: " );
		scanf( "%d", &ejercicio[i].dni );
		printf( "Ingrese el nombre del alumno: ");
		scanf( "%s", &ejercicio[i].nombre);
		printf( "Ingrese el apellido del alumno: " );
		scanf( "%s", &ejercicio[i].apellido );
		printf( "Ingrese la edad del alumno: " );
		scanf( "%d", &ejercicio[i].edad );
		printf( "Ingrese el promedio del alumno: " );
		scanf( "%f", &ejercicio[i].promedio );
		
		
		
	}
	
	system("cls");
	
	printf("\tLista de Alumno\n");
	for(int i=0;i<3;i++){
		printf("DNI\t   Apellido\t  Nombre\t  Promedio\n");
		printf("%d\t    %s\t  %s\t   %.2f\n", ejercicio[i].dni,ejercicio[i].apellido,ejercicio[i].nombre,ejercicio[i].promedio);
		total=i+1;
		total_edad=total_edad+ejercicio[i].edad;
		if(ejercicio[i].edad>mayor) mayor=ejercicio[i].edad;
		if(ejercicio[i].edad<menor) menor=ejercicio[i].edad;
		prom_notas=prom_notas+ejercicio[i].promedio;
		if(ejercicio[i].promedio>=7) prom_mayor++;
		if(ejercicio[i].promedio<7) prom_menor++;
		
	}
	printf("Total alumnos:%d \n",total);
	prom=total_edad/3;
	printf("Promedio de edad de los alumnos: %.2f", prom);
	printf("\nMayor edad: %d",mayor);
	printf("\nMenor edad: %d",menor);
	printf("\n---------------------------\n");
	prom_notas=prom_notas/3;
	
	printf("Promedio general de los alumnos: %.2f\n",prom_notas);
	printf("Cantidad de alumnos con promedio mayor e igual a 7: %d\n",prom_mayor);
	printf("Cantidad de alumnos con promedio menor a 7: %d\n",prom_menor);
	porcentaje=(prom_mayor*100)/3;
	printf("Porcentaje de alumnos con promedio mayor e igual a 7: %d\n",porcentaje);
	prom_alumnos=prom_menor/3.0;
	printf("Promedio de alumnos con promedio menor a 7: %.2f",prom_alumnos);
	
/*#include <stdio.h>
struct estructura_amigo {	//Establece al estructura estructura_amigo
	char nombre[30];
	char apellido[40];
	char telefono[10];
	int edad;
};
struct estructura_amigo amigo = {	//Declarara estructura_amigo
	"Juanjo",
		"L�pez",
		"983403367",
		30
};
int main()
{
	printf( "%s tiene ", amigo.apellido );	//Muestra por pantalla los datos de la estructura
	printf( "%i a�os ", amigo.edad );
	printf( "y su tel�fono es el %s.\n" , amigo.telefono );
	return 0;
} */
	
	/*
	
	TEORIA
	1-Su funci�n es asignar un nombre alternativo a tipos existentes, 
	a menudo cuando su declaraci�n normal es aparatosa, potencialmente confusa o 
	probablemente variable de una implementaci�n a otra.
	2- 
	#define SIZE_FIRST 100
	#define SIZE_LAST 200
	#define NUM_CONTACTS 100
	
	//Definici�n de la estructura 
	struct contact_information 
	{
		char firstname[SIZE_FIRST];
		char lastname[SIZE_LAST];
		unsigned int homephone;
		unsigned int mobilephone;
	};
	// Declaraci�n de variable 
	struct contact_information person2;
	// Definici�n del sin�nimo 
	typedef struct contact_information contact_info;
	//Declaraci�n utilizando el sin�nimo 
	contact_info person1, contacts[NUM_CONTACTS];
	3-Una estructura puede estar dentro de otra estructura a esto se le conoce como anidamiento 
	o estructuras anidadas. Ya que se trabajan con datos en estructuras si definimos un tipo de dato 
	en una estructura y necesitamos definir ese dato dentro de otra estructura 
	solamente se llama el dato de la estructura anterior.
	EJEMPLO
	struct empleado /* se crea nuevamente la estructura 
	{
		char nombre_empleado[25];
		/* creamos direcc_empleado con "struct" del tipo "estructura infopersona" 
		struct infopersona direcc_empleado; 
		double salario;
	};
	
	struct cliente /* se crea nuevamente la estructura 
	{
		char nombre_cliente[25];
		/* creamos direcc_cliente con "struct" del tipo "estructura infopersona" 
		struct infopersona direcc_cliente;
		double saldo;
	};
	
	
	
	*/
	

	
	
	
	
	
	
	
	
	
	
	
	*/
