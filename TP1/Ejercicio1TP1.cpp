registro1 //Valido
1registro //No es valido porque no se permite poner numeros primero
archivo //Valido
return // No es valido porque return es una palabra reservada del lenguaje
$impuesto //No es valido porque el primer caracter debe ser una letra o un caracter subrayado
nombre //Valido
nombre y direccion //No es valido porque no se pueden ocupar espacios
nombre_y_direccion //Valido
nombre-y-direccion //No es valido porque no se pueden ocupar guiones medios
123-45-6789 //No es valido porque no se pueden ocupar guiones medios, ni tampoco numeros primero
