#include <stdio.h>

int main() {
	int a;
	float b;
	char c;
	a=7;
	b=8.2;
	c='s';
	//La diferencia es que este muestra por pantalla todo junto el codigo
	printf("%d%f%c", a,b,c); 
	//En cambio este, muestra por pantalla las variables separadas
	printf("%d \t %f \t %c", a,b,c); 
	//\t seria como teclear una vez la tecla de tabulacion, asi los resultados salen separados
	return 0;
}

