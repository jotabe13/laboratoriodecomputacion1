#include <stdio.h>
#include <stdlib.h>
#define col 3
#define fila 3


void suma(int Matriz1[fila][col], int Matriz2[fila][col]);
void resta(int Matriz1[fila][col], int Matriz2[fila][col]);
void multiplicacion(int Matriz1[fila][col], int Matriz2[fila][col]);
void comparacion(int Matriz1[fila][col], int Matriz2[fila][col]);
void imprimir(int Matriz[fila][col]);


main()
{
	int matrizN[fila][col], matrizM[fila][col], columnas, filas, i=0,j=0;
	float num;
	printf("Matriz M\n");
	for(i=0;i<fila;i++){
		for(j=0;j<col;j++){
			printf("Ingrese el numero que desea, se va a agregar en la posicion %d,%d:", i,j);
			scanf("%i", &matrizM[i][j]);
		}
		putchar('\n');
	}
	
	printf("Matriz N\n");
	for(i=0;i<fila;i++){
		for(j=0;j<col;j++){
			printf("Ingrese el numero que desea, se va a agregar en la posicion %d,%d:", i,j);
			scanf("%i", &matrizN[i][j]);
		}
		putchar('\n');
	}
	
	system("cls");
	
	system("cls");
	comparacion(matrizM, matrizN);
	printf("Matriz M:\n\n");
	imprimir(matrizM);
	printf("\nMatriz N:\n\n");
	imprimir(matrizN);
	suma(matrizM, matrizN);
	resta(matrizM, matrizN);
	multiplicacion(matrizM, matrizN);
}
void comparacion(int Matriz1[3][3], int Matriz2[3][3])
{
	int columnas, filas, iguales=0;
	for (columnas = 0; columnas < 3; columnas++)
	{
		for (filas = 0; filas < 3; filas++)
		{
			if (Matriz1[filas][columnas] == Matriz2[filas][columnas]) iguales++;
		}
	}
	if (iguales == 9) printf("\n\nAmbas matrices son iguales\n\n");
	else printf("\n\nLas matrices son diferentes\n\n");
}
void imprimir(int Matriz[3][3])
{
	int columnas, filas;
	for (filas = 0; filas < 3; filas++)
	{
		for (columnas = 0; columnas < 3; columnas++)
		{
			printf("[%d]\t", Matriz[filas][columnas]);
		}
		printf("\n");
	}
}
void suma(int Matriz1[3][3], int Matriz2[3][3])
{
	int Matriz_suma[3][3], columnas, filas;
	for (columnas = 0; columnas < 3; columnas++)
	{
		for (filas = 0; filas < 3; filas++)
		{
			Matriz_suma[filas][columnas] = Matriz1[filas][columnas] + Matriz2[filas][columnas];
		}
	}
	printf("\nMatriz suma:\n\n");
	imprimir(Matriz_suma);
}
void resta(int Matriz1[3][3], int Matriz2[3][3])
{
	int Matriz_resta[3][3], columnas, filas;
	for (columnas = 0; columnas < 3; columnas++)
	{
		for (filas = 0; filas < 3; filas++)
		{
			Matriz_resta[filas][columnas] = Matriz1[filas][columnas] - Matriz2[filas][columnas];
		}
	}
	printf("\nMatriz resta:\n\n");
	imprimir(Matriz_resta);
}
void multiplicacion(int Matriz1[3][3], int Matriz2[3][3])
{
	int Matriz_multiplicacion[3][3], columnas, filas, cont, multiplicacion;
	for (columnas = 0; columnas < 3; columnas++)
	{
		for (filas = 0; filas < 3; filas++)
		{
			multiplicacion=0;
			for (cont = 0; cont < 3; cont++)
			{
				multiplicacion = multiplicacion + (Matriz1[filas][cont] * Matriz2[cont][columnas]);
			}
			Matriz_multiplicacion[filas][columnas] = multiplicacion;
		}
	}
	printf("\nMatriz multiplicación:\n\n");
	imprimir(Matriz_multiplicacion);
}
