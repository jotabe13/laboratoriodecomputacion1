#include <stdio.h>

int main() {
	float precio,descuento,resultado;
	int cantidad;
    printf("Ingrese el precio del producto: ");
	scanf("%f", &precio);
	printf("Ingrese la cantidad de unidades: ");
	scanf("%d", &cantidad);
	resultado=precio*cantidad;
	descuento=resultado*0.15;
	resultado=resultado-descuento;
	printf("El precio a pagar es: %.2f", resultado);
	return 0;
}

