#include<stdio.h>
#include <stdbool.h>


int main()
{
	int A[11]= {1,4,6,6,8,10,11,11,15,16,16};
	int bandera=0;
	int min,max,mitad,usuario,n=11,cont=0;
	min=0;
	max=n;
	
	printf("Ingrese el numero que desea buscar: \n");
	scanf("%d",&usuario);
	
	
	while ((min<=max) && (cont<11)){
		mitad=(min+max)/2;
		if ((A[mitad]==usuario)&&(A[mitad+1]==usuario)){
			bandera=1;
			break;
		}
		else if (A[mitad]==usuario){
			
			bandera=2;
			break;
		}
		if (A[mitad]>usuario){
			max=mitad;
			mitad=(min+max)/2;
		}
		if (A[mitad]<usuario){
			min=mitad;
			mitad=(min+max)/2;
		}
		cont++;
	}
	
	
	if(bandera==2){
		printf("El numero: %d fue encontrado en la posicion %d", usuario, mitad);
	}
	if(bandera==1){
		printf("El numero %d fue encontrado en la posicion: %d y en la posicion %i",usuario,mitad, (mitad+1));
	}
	if(bandera==0){
		printf("No se encuentra el numero");
	}
	
	return 0;
}
