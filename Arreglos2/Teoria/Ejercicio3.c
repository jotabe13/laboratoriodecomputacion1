#include <stdio.h>
#include <stdlib.h>


int cmpfunc (const void * a, const void * b);
void swap(int *a, int *b);
void monticulo(int arr[], int n, int i);
void heapSort(int arr[], int n);
void imprimir_Array(int arr[], int n);
void shellSort(int array[], int n);
void merge(int arr[], int p, int q, int r);
void mergeSort(int arr[], int l, int r);
	
	




int main(int argc, char *argv[]) {
	
	int a[]={6,3,2,8,9,11,24,33,29,5};
	int i=0,j=0,aux=0,temp=0,minimo=0,n=10,x=0,y=0;
	int usuario=0;
	printf("Ingrese el metodo de ordenamiento que desea: \n");
	printf("1-Insercion\n");
	printf("2-Burbuja\n");
	printf("3-Seleccion\n");
	printf("4-Rapido(Quicksort)\n");
	printf("5-Fusion(Merge)\n");
	printf("6-Monticulo(Heap)\n");
	printf("7-Shell\n\n");
	scanf("%d",&usuario);
	
	
	
	
	
	switch(usuario){
	case 1:
		
		printf("Ha seleccionado el metodo insercion.\n");
		for(i=1;i<n;i++)
		{
			j=i;
			aux=a[i];
			while(j>0 && aux<a[j-1])
			{
				a[j]=a[j-1];
				j--;
			}
			a[j]=aux;
		}
		for(i=0;i<10;i++)
		{
			printf("%d/",a[i]);
		}
		break;
		
	case 2:
		printf("Ha seleccionado el metodo burbuja.\n");
		for(i=0;i<=n;i++){
			for(j=0;j<n-1;j++){
				if(a[j]>a[j+1]){
					aux=a[j];
					a[j]=a[j+1];
					a[j+1]=aux;
				}
			}
		}
		for(i=0;i<10;i++)
		{
			printf("%d/",a[i]);
		}
		break;
	case 3:
		printf("Ha seleccionado el metodo seleccion.\n");
		for(i=0;i<n-1;i++)
		{
			minimo=i;
			for(j=i+1;j<n;j++)
			{
				if(a[minimo] > a[j])
				{
					minimo=j;
				}
			}
			temp=a[minimo];
			a[minimo]=a[i];
			a[i]=temp;
		}
		for(i=0;i<n;i++)
		{
			printf("%d/",a[i]);
		}
		break;
	case 4:
		printf("Ha seleccionado el metodo rapido.\n");
		
		qsort(a, 10, sizeof(int), cmpfunc);
		for( n = 0 ; n < 10; n++ ) {   
			printf("%d/", a[n]);
		}
		break;
	case 5:
		printf("Ha seleccionado el metodo fusion.\n");
		
		int size = sizeof(a) / sizeof(a[0]);
		
		mergeSort(a, 0, size - 1);
		imprimir_Array(a, size);
		break;
	case 6:
		printf("Ha seleccionado el metodo monticulo.\n");
		
		int n = sizeof(a) / sizeof(a[0]);
		
		heapSort(a, n);
		imprimir_Array(a, n);
		break;
	case 7:
		printf("Ha seleccionado el metodo shell.\n");
		
		size = sizeof(a) / sizeof(a[0]);
		shellSort(a, size);
		imprimir_Array(a, size);
		break;
	default:
		printf("No ha seleccionado ninguno. Vuelva a intentar.");
	}
	
	return 0;
}

int cmpfunc (const void * a, const void * b) {
	return ( *(int*)a - *(int*)b );
}

void swap(int *a, int *b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}

void monticulo(int arr[], int n, int i) {
	
	int mas_largo = i;
	int izq = 2 * i + 1;
	int der = 2 * i + 2;
	
	if (izq < n && arr[izq] > arr[mas_largo])
		mas_largo = izq;
	
	if (der < n && arr[der] > arr[mas_largo])
		mas_largo = der;
	
	
	if (mas_largo != i) {
		swap(&arr[i], &arr[mas_largo]);
		monticulo(arr, n, mas_largo);
	}
}


void heapSort(int arr[], int n) {
	
	for (int i = n / 2 - 1; i >= 0; i--)
		monticulo(arr, n, i);
	
	
	for (int i = n - 1; i >= 0; i--) {
		swap(&arr[0], &arr[i]);
		monticulo(arr, i, 0);
	}
}


void imprimir_Array(int arr[], int n) {
	for (int i = 0; i < n; ++i)
		printf("%d/", arr[i]);
}

void shellSort(int array[], int n) {
	
	
	for (int gap = n / 2; gap > 0; gap /= 2) {
		for (int i = gap; i < n; i += 1) {
			int temp = array[i];
			int j;
			for (j = i; j >= gap && array[j - gap] > temp; j -= gap) {
				array[j] = array[j - gap];
			}
			array[j] = temp;
		}
	}
}


void merge(int arr[], int p, int q, int r) {
	

	int n1 = q - p + 1;
	int n2 = r - q;
	
	int L[n1], M[n2];
	
	for (int i = 0; i < n1; i++)
		L[i] = arr[p + i];
	for (int j = 0; j < n2; j++)
		M[j] = arr[q + 1 + j];
	
	
	int i, j, k;
	i = 0;
	j = 0;
	k = p;
	
	
	while (i < n1 && j < n2) {
		if (L[i] <= M[j]) {
			arr[k] = L[i];
			i++;
		} else {
			arr[k] = M[j];
			j++;
		}
		k++;
	}
	
	
	while (i < n1) {
		arr[k] = L[i];
		i++;
		k++;
	}
	
	while (j < n2) {
		arr[k] = M[j];
		j++;
		k++;
	}
}


void mergeSort(int arr[], int l, int r) {
	if (l < r) {
		
		
		int m = l + (r - l) / 2;
		
		mergeSort(arr, l, m);
		mergeSort(arr, m + 1, r);
		
		merge(arr, l, m, r);
	}
}


