#include <stdio.h>

int main(int argc, char *argv[]) {
	
	int a[8]= {1,4,6,8,10,11,15,16};
	int usuario=0;
	printf("Ingrese el numero que desea buscar: \n");
	scanf("%d", &usuario);
	for(int i=0; i<8; i++){
		if(usuario==a[i]){
			printf("El numero %d fue encontrado en la posicion %d", usuario, i);
			break;
		}
	}
	
	// No, no es necesario, una vez que se encuentra el numero buscado no es necesario.
	//Busqueda comun se puede usar para cualquier arrgelo, busqueda binaria no se puede utilizar debido a que el arreglo debe de estar ordenado.
	//En los casos donde tengas un arreglo muy grande(es mas eficiente que busqueda comun), pero si o si tiene que estar ordenado el arreglo.
	
	return 0;
}

