#include <stdio.h>

void ejercicio3(int i);

int main(int argc, char *argv[]) {
	int i=0; 
	
	printf("Cuantos numeros desea ingresar? ");
	scanf("%d", &i);
	ejercicio3(i);
	
	
	return 0;
}

void ejercicio3(int i){
	int cont=0, mayor=0, menor=999999999, usuario=0;
	float resultado=0;
	while(cont<i){
		
		printf("Ingrese un numero: ");
		scanf("%d", &usuario);
		
		if(usuario>mayor){
			mayor=usuario;
		}
		
		if(usuario<menor){
			menor=usuario;
		}
		resultado=resultado+usuario;
		
		cont++;
		
	}
	
	resultado=resultado/i;
	printf("El numero mas grande introducido es %d\n", mayor);
	printf("El numero mas chico ingresado es %d\n", menor);
	printf("El promedio es: %.2f", resultado);
	
}

