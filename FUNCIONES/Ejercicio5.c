#include <stdio.h>
#include <stdlib.h>
void factorial();
int main(int argc, char *argv[])
{
	factorial();
	
	return 0;
}

void factorial(){
	
	int resultado=0;
	int factorial;
	
	printf("Introduzca No. para calcular el factorial ...: ");
	scanf(" %ld", &factorial);
	
	resultado = 1;
	while(factorial > 1) {
		resultado *= factorial;
		printf(" %ld x",factorial);
		factorial--;
	}
	printf(" 1 = %ld\n\n",resultado);
	
}
