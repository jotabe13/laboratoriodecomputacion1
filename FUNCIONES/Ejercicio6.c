#include <stdio.h>
#include <conio.h>

int PRIMO(int n){
	
	int div = 2;
	int primo = 1;
	
	while ((primo != 0) && (div < n)){
		if (n % div == 0) primo = 0;
		div++;
	}
	
	return primo;
	
}

int main(){
	
	int num;
	int es_primo = 0;
	int cont_primo;
	
	cont_primo = 0;
	
	for (int i = 1; i > 0; i++ ){
		
		printf("Ingrese un numero: ");
		scanf ("%d", &num);
	
		if (num == 0) break;
		
		es_primo = PRIMO(num);
		
		if (es_primo == 1) cont_primo++;
		
	}
	

	
	printf("Se ingresaron %d numeros primos", cont_primo);

	
}
